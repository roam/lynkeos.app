Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Lynkeos
Source: http://lynkeos.sourceforge.net/
Files-Excluded:
# Non-free image Help/project-support.jpg
 application/Help
# Unclear licensing terms; unused
 help
# Bundled third-party source; unused
 application/ThirdPartySources/dcraw.c
# Unused; just causes lintian P: source-contains-empty-directory
 application/SDK

Files: *                       
Copyright: 1998-2020 Jean-Etienne Lamiaud
           2004-2005 Christophe Jalady
License: GPL-2+

Files: application/it.lproj/*
Copyright: 2004-2005 Diego Meozzi
License: GPL-2+

Files: application/es.lproj/*
Copyright: 2005-2020 Laurence Bourcier-Huet and Jean-Etienne Lamiaud
License: GPL-2+

Files: application/ThirdPartySources/*
Copyright: 2003-2008, Snowmint Creative Solutions LLC
License: BSD-3-clause

Files: debian/*
Copyright: 2005-2021 Debian GNUstep maintainers
           2006 Julien Danjou <acid@debian.org>
           2018 Frédéric Bonnard <frediz@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: BSD-3-clause
 Copyright (c) 2003-2008, Snowmint Creative Solutions LLC
 http://www.snowmintcs.com/
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of Snowmint Creative Solutions LLC nor the names
    of its contributors may be used to endorse or promote products
    derived from this software without specific prior written
    permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
